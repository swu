# This file is part of swu
# Copyright (C) 2016, 2019 Sergey Poznyakoff
#
# Swu is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Swu is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with swu.  If not, see <http://www.gnu.org/licenses/>.

CFLAGS=
PREFIX=/usr/local
MANDIR=$(PREFIX)/share/man
BINDIR=$(PREFIX)/bin
MAN1DIR=$(MANDIR)/man1

PACKAGE=swu
VERSION=1.0
DISTDIR=$(PACKAGE)-$(VERSION)

DISTFILES=Makefile swu.c swu.1 README NEWS

swu: swu.c
	cc -oswu $(CFLAGS) $(CPPFLAGS) swu.c

clean:
	rm -f *.o swu

install: swu
	install -D swu $(DESTDIR)$(BINDIR)/swu
	install -D -m 644 swu.1 $(DESTDIR)$(MAN1DIR)/swu.1

DISTDIR:
	rm -rf $(DISTDIR) && \
	mkdir $(DISTDIR) &&  \
	cp $(DISTFILES) $(DISTDIR)

dist: DISTDIR
	tar cfz $(DISTDIR).tar.gz $(DISTDIR)
	rm -rf $(DISTDIR)

distcheck: dist
	tar xfz $(DISTDIR).tar.gz
	@if $(MAKE) -C $(DISTDIR) $(DISTCHECKFLAGS); then \
	  echo "$(DISTDIR).tar.gz ready for distribution"; \
	  rm -rf $(DISTDIR); \
        else \
          exit 2; \
        fi
